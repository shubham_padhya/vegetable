/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vegretablesimuilation;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class VegretableSimuilation {
    
   public static void main(String[] args) {
       VegetableFactory factory = VegetableFactory.getInstance();
       
       Vegetable carrot1 = new Carrot("orange", 1.6);
       
       Vegetable carrot2 = new Carrot("green", 0.6);
       
       Vegetable beet1 = new Beet("pink", 1.4);
       Vegetable beet2 = new Beet("green", 1.0);
       
       
       System.out.println( "The carrot is" + carrot1.getSize() + "and the color is" + carrot1.getColor());
       System.out.println( "The carrot is" + carrot1.isRipe());
       System.out.println( "The carrot is" + carrot2.getSize() + "and the color is" + carrot2.getColor());
       System.out.println( "The carrot is" + carrot2.isRipe());
       
       System.out.println( "The Beet is" + beet1.getSize() + "and the color is" + beet1.getColor());
       System.out.println( "The carrot is" + beet1.isRipe());
       System.out.println( "The carrot is" + beet2.getSize() + "and the color is" + beet2.getColor());
       System.out.println( "The carrot is" + beet2.isRipe());
   }

    private static class Carrot extends Vegetable {

        public Carrot(String orange, double d) {
        }
    }

    private static class Beet extends Vegetable {

        public Beet(String pink, double d) {
        }
    }

    private static class Vegetable {

        public Vegetable() {
        }

        private boolean getColor() {
            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        private String getSize() {
            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        private String isRipe() {
            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }
    }
}

 class VegetableFactory {
    
    private static VegetableFactory factory;

    private VegetableFactory() { 
    }
    
    public static VegetableFactory getInstance(){ 
    {
        if (factory==null)
            factory = new VegetableFactory();

    }
        return factory;
    }
   
      public Vegetable getVegetable(String color, double size){
        if(color=="orange" && size>=1.5){
            return new Carrot(color, size);
        }
        
        if(color=="green" && size<=1){
            return new Carrot(color, size);
        }
        if(color=="pink" && size>=1.4){
            return new Beet(color, size);  
    }
        
        if(color=="pink" && size<=1){
            return new Beet(color, size);  
    }
        return null;
    }

    private static class Beet extends Vegetable {

        public Beet(String color, double size) {
        }
    }

    private static class Carrot extends Vegetable {

        public Carrot(String color, double size) {
        }
    }

    private static class Vegetable {

        public Vegetable() {
        }
    }
 }



